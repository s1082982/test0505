﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace test0505.Controllers
{
    public class BMIController : Controller
    {
        public ActionResult Index()
        {
            return View(new BMIData());
        } 

        [HttpPost]
        // GET: BMI
        public ActionResult Index(BMIData data)
        {


            if (ModelState.IsValid)
            {
                var m_height = data.Height / 100;
                var result = data.Weight / (m_height * m_height);

                if(result < 18.5)
                {
                    data.Level = "體重過輕";
                }
                else if (18.5 <= result  && result < 24)
                {
                    data.Level = "正常範圍";
                }
                else if (24 <= result && result < 27)
                {
                    data.Level = "過    重";
                }
                else if (27 <= result && result < 30)
                {
                    data.Level = "輕度肥胖";
                }
                else if (30 <= result && result < 35)
                {
                    data.Level = "中度肥胖";
                }
                else if (result > 35)
                {
                    data.Level = "重度肥胖";
                }

                data.Height = data.Height;
                data.Weight = data.Weight;

                data.Result = result;
            }

            return View(data);
        }
    }
}