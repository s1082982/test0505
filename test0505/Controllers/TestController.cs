﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace test0505.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        public ActionResult Index()
        {
            ViewData["aaa"] = "hello world";
            ViewBag.name ="jade";
            ViewData["C"] = 1;
            ViewData["D"] = 12;
            ViewBag.A = 12;
            ViewBag.B = 40;

            return View();
        }

        public ActionResult Html()
        {
            return View();
        }

        public ActionResult HtmlHelper()
        {
            return View();
        }
        public ActionResult Razor()
        {
            return View();
        }
    }
}